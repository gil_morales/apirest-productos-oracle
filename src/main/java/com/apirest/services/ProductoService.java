package com.apirest.services;

import com.apirest.models.Producto;
import com.apirest.models.ProductoList;
import com.apirest.repositories.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {
    @Autowired
    ProductoRepository productoRepository;

    public ProductoList getProductos(){
        ProductoList productoList = new ProductoList();
        List<Producto> productListDao = this.productoRepository.findAll();
        for(Producto p : productListDao){
            productoList.getProducts().add(p);
        }
        return  productoList;
    }

    public Producto getProducto(Long id) {
        return this.productoRepository.findById(id);
    }

    public Producto addProducto(Producto producto){
        return this.productoRepository.save(producto);
    }

    public Long updateProducto(Long id, Producto producto){
        Producto p =  this.productoRepository.findById(id);
        p.setDescripcion(producto.getDescripcion());
        p.setPrecio(producto.getPrecio());
        this.productoRepository.save(p);
        return p.getId();
    }

    public Long deleteProducto(Long id){
        return this.productoRepository.deleteById(id);
    }
}
