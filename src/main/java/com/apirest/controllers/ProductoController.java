package com.apirest.controllers;

import com.apirest.models.Producto;
import com.apirest.models.ProductoList;
import com.apirest.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {
    @Autowired
    private ProductoService productoService;

    @GetMapping("/productos")
    public ProductoList getProducts(){
        return this.productoService.getProductos();
    }

    @GetMapping("/productos/{id}")
    public Producto getProducto(@PathVariable("id") Long id){
        return this.productoService.getProducto(id);
    }

    @PostMapping("/productos")
    public ResponseEntity addProducto(@RequestBody Producto producto){
        this.productoService.addProducto(producto);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.OK);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable("id") Long id, @RequestBody Producto producto){
        this.productoService.updateProducto(id, producto);
        return new ResponseEntity<>("Producto actualizado correctamente", HttpStatus.OK);
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable("id") Long id){
        this.productoService.deleteProducto(id);
        return new ResponseEntity<>("Producto eliminado correctamente", HttpStatus.OK);
    }
}
